<?php

return array(
    'zf-oauth2' => array(
        'allow_implicit' => false, // default (set to true when you need to support browser-based or mobile apps)
        'access_lifetime' => 3600, // default (set a value in seconds for access tokens lifetime)
        'enforce_state'  => true,  // default
        'storage'        => 'ZF\OAuth2\Doctrine\Adapter\DoctrineAdapter', // service name for the OAuth2 storage adapter
        'options' => array(
            'always_issue_new_refresh_token' => true,
        ),
    ),
);
