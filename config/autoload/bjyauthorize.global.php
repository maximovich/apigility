<?php

return [
    'bjyauthorize' => array(
        // Using the authentication identity provider, which basically reads the roles from the auth service's identity
        'identity_provider' => 'BjyAuthorize\Provider\Identity\AuthenticationIdentityProvider',

        'role_providers'        => array(
            // using an object repository (entity repository) to load all roles into our ACL
            'BjyAuthorize\Provider\Role\ObjectRepositoryProvider' => array(
                'object_manager'    => 'doctrine.entitymanager.orm_default',
                'role_entity_class' => 'Db\Entity\Role',
            ),
        ),

        'rule_providers' => [
            'BjyAuthorize\Provider\Rule\Config' => [
                'allow' => [
                    [['administrator'], 'administration', 'access'],
                ],
#                'deny' => [
#                ],
            ],
        ],

        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
#                'report' => [],
#                'team' => [],
                'administration' => [],
            ],
        ],

        'unauthorized_strategy' => 'BjyAuthorizeViewRedirectionStrategy',
        'default_role' => 'guest',

        'guards' => array(
            'BjyAuthorize\Guard\Route' => array(
                // ZFC User
                array('route' => 'zfcuser', 'roles' => array('user')),
                array('route' => 'zfcuser/login', 'roles' => array('guest')),
                array('route' => 'zfcuser/logout', 'roles' => array('guest')),
                array('route' => 'zfcuser/authenticate', 'roles' => array('guest')),
                array('route' => 'zfcuser/register', 'roles' => array('guest')),
                array('route' => 'zfcuser/changepassword', 'roles' => array('user')),
                array('route' => 'zfcuser/changeemail', 'roles' => array('user')),

                // ZFCampus OAuth2
                array('route' => 'oauth', 'roles' => array('guest')),
                array('route' => 'oauth/authorize', 'roles' => array('user')),
                array('route' => 'oauth/resource', 'roles' => array('user')),
                array('route' => 'oauth/code', 'roles' => array('guest')),
#                array('route' => 'oauth/receivecode', 'roles' => array('user')),

                // Skeleton
                array('route' => 'user/client', 'roles' => array('user')),


                // API
                array('route' => 'api.rest.doctrine.o-auth2-client', 'roles' => array('guest')),
                array('route' => 'api.rpc.me', 'roles' => array('guest')),

                // ZFC User Admin
                array('route' => 'zfcadmin', 'roles' => array('administrator')),
                array('route' => 'zfcadmin/zfcuseradmin', 'roles' => array('administrator')),
                array('route' => 'zfcadmin/zfcuseradmin/list', 'roles' => array('administrator')),
                array('route' => 'zfcadmin/zfcuseradmin/create', 'roles' => array('administrator')),
                array('route' => 'zfcadmin/zfcuseradmin/edit', 'roles' => array('administrator')),
                array('route' => 'zfcadmin/zfcuseradmin/remove', 'roles' => array('administrator')),
            ),
        ),
    ),
];