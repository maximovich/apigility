<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Console\Console;
use Application\Controller\RedirectCallback;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ServiceProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $serviceManager = $e->getApplication()->getServiceManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_ROUTE, function($e) use ($serviceManager) {
            $routeMatch = $e->getRouteMatch();

            $auth = $serviceManager->get('zfcuser_auth_service');

            // BjyAuthorize is not included when ran from the command line
            if (!Console::isConsole()) {
                try {
                    $service = $serviceManager->get('BjyAuthorize\Service\Authorize');
                    $match = $routeMatch;
                    $routeName  = $match->getMatchedRouteName();
                    $isAllowed = $service->isAllowed('route/' . $routeName);
                } catch (\Exception $e) {
                    $isAllowed = true;
                }
            }

            if (!$auth->hasIdentity()
                && $routeMatch->getMatchedRouteName() != 'user/login'
                && $routeMatch->getMatchedRouteName() != 'zfcuser/login'
                && !Console::isConsole()
                && !$isAllowed
                ) {

                // generate redirect url from requested route
                $redirect = $e->getRouter()->assemble(
                    $routeMatch->getParams(),
                    array(
                        'name' => $routeMatch->getMatchedRouteName(),
                    )
                );
                $redirect .= '?' . $_SERVER['QUERY_STRING'];

                $response = $e->getResponse();
                $response->getHeaders()->addHeaderLine(
                    'Location',
                    $e->getRouter()->assemble(
                        array(),
                        array(
                            'name' => 'zfcuser/login',
                            'query' => array( 'redirect' => $redirect )
                        )
                    )
                );
                $response->setStatusCode(302);
                return $response;
            }
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'zfcuser_redirect_callback' => function ($sm) {
                    /* @var RouteInterface $router */
                    $router = $sm->get('Router');
                    /* @var Application $application */
                    $application = $sm->get('Application');
                    /* @var ModuleOptions $options */
                    $options = $sm->get('zfcuser_module_options');

                    return new RedirectCallback($application, $router, $options);
                }
            ),
        );
    }
}