<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Console\Controller\User' => 'Console\Controller\UserController',
        ),
    ),

    'console' => array(
        'router' => array(
            'routes' => array(
                'create-administrative-user' => array(
                    'options' => array(
                        'route'    => 'skeleton:create:administrator',
                        'defaults' => array(
                            'controller' => 'Console\Controller\User',
                            'action'     => 'createAdministrator'
                        ),
                    ),
                ),
            ),
        ),
    ),
);
