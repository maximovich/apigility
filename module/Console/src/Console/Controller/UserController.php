<?php

namespace Console\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Console\Request as ConsoleRequest;
use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Console\ColorInterface as Color;
use Zend\Console\Prompt;
use Db\Entity\User;
use RuntimeException;
use ZF\OAuth2\Doctrine\Entity;
use Zend\Crypt\Password\Bcrypt;

class UserController extends AbstractActionController
{
    public function createAdministratorAction()
    {
        $console = $this->getServiceLocator()->get('console');
        $zfcuserOptions = $this->getServiceLocator()->get('zfcuser_module_options');
        $objectManager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');

        // Make sure that we are running in a console and the user has not tricked our
        // application into running this action from a public web server.
        $request = $this->getRequest();
        if (!$request instanceof ConsoleRequest){
            throw new RuntimeException('You can only use this action from a console.');
        }

        $displayName = Prompt\Line::prompt("Administrator Display Name: ", false, 255);
        $username = Prompt\Line::prompt("Administrator Username: ", false, 255);
        $email = Prompt\Line::prompt("Administrator Email: ", false, 255);

        $password = '';
        $passwordVerify = false;
        while ($password !== $passwordVerify) {
            $passwordPrompt = new Prompt\Password();
            $password = $passwordPrompt->show();
            $passwordPrompt = new Prompt\Password("Verify Password: ");
            $passwordVerify = $passwordPrompt->show();

            if ($password !== $passwordVerify) {
                $console->write("Password verification does not match.  Please try again.\n", Color::YELLOW);
            }
        }

        $user = new User();
        $user->setDisplayName($displayName);
        $user->setUsername($username);
        $user->setEmail($email);
        $bcrypt = new Bcrypt();
        $bcrypt->setCost($zfcuserOptions->getPasswordCost());
        $user->setPassword($bcrypt->create($password));

        // Add all roles to administrative user
        $roles = $objectManager->getRepository('Db\Entity\Role')->findAll();
        foreach ($roles as $role) {
            $role->addUser($user);
            $user->addRole($role);
        }

        $objectManager->persist($user);

        // Add a default client for managing API clients
        $client = new Entity\Client();
        $client->setClientId($user->getEmail());
        $client->setSecret($bcrypt->create($password));
        $client->setUser($user);

        foreach (array('client-create', 'client-read', 'client-update', 'client-delete') as $scopeName) {
            $scope = $objectManager->getRepository('ZF\OAuth2\Doctrine\Entity\Scope')->findOneBy(array(
                'scope' => $scopeName,
            ));

            $scope->addClient($client);
            $client->addScope($scope);
        }

        $objectManager->persist($client);
        $objectManager->flush();

        $console->write("The administrator '$displayName' has been created.\n", Color::GREEN);

        return new ViewModel();
    }


}

