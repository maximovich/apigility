<?php

namespace Db\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use ZF\OAuth2\Doctrine\Entity;

class OAuth2Scope implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $scope = new Entity\Scope();
        $scope->setScope('client-create');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('client-read');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('client-update');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('client-delete');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('data-create');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('data-read');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('data-update');
        $manager->persist($scope);

        $scope = new Entity\Scope();
        $scope->setScope('data-delete');
        $manager->persist($scope);

        $manager->flush();
    }
}
