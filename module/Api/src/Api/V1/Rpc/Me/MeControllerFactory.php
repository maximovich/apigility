<?php
namespace Api\V1\Rpc\Me;

class MeControllerFactory
{
    public function __invoke($controllers)
    {
        $controller = new MeController();
        $controller->setObjectManager(
            $controllers->getServiceLocator()->get('doctrine.entitymanager.orm_default')
        );

        return $controller;
    }
}
