<?php
namespace Api\V1\Rpc\Me;

use Zend\Mvc\Controller\AbstractActionController;
use DoctrineModule\Persistence\ProvidesObjectManager;
use ZF\ApiProblem\Exception\InvalidArgumentException;
use ZF\ContentNegotiation\ViewModel as ContentNegotiationViewModel;

class MeController extends AbstractActionController
{
    use ProvidesObjectManager;

    public function meAction()
    {
        $user = $this->getIdentity()->getAuthenticationIdentity();
        if (!$user) {
            throw new InvalidArgumentException('OAuth2 token is invalid');
        }

        $userEntity = $this->getObjectManager()->getRepository('Db\Entity\User')->find($user['user_id']);

        $userData = $userEntity->getArrayCopy();
        unset($userData['password']);

        $viewModel = new ContentNegotiationViewModel(array(
            'payload' => $userData,
        ));

        return $viewModel;
    }
}
