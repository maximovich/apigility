<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2015 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Api\Provider\UserId;

use Zend\Stdlib\RequestInterface;
use ZF\OAuth2\Provider\UserId\UserIdProviderInterface;

class ZfcAuthenticationService implements UserIdProviderInterface
{
    /**
     * @var ZendAuthenticationService
     */
    private $authenticationService;

    /**
     *  Set authentication service
     *
     * @param ZendAuthenticationService $service
     */
    public function __construct($service)
    {
        $this->authenticationService = $service;
    }

    /**
     * Use Zend\Authentication\AuthenticationService to fetch the identity.
     *
     * @param RequestInterface $request
     * @return mixed
     */
    public function __invoke(RequestInterface $request)
    {
        return $this->authenticationService->getIdentity()->getId();
    }
}
