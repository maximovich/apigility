<?php

namespace Api\Query\CreateFilter\OAuth2;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\ResourceEvent;
use ZF\Apigility\Doctrine\Server\Query\CreateFilter\AbstractCreateFilter;
use Zend\Crypt\Password\Bcrypt;

/**
 * Class DefaultCreateFilter
 *
 * @package ZF\Apigility\Doctrine\Server\Query\CreateFilter
 */
class ClientCreateFilter extends AbstractCreateFilter
{
    /**
     * @param string $entityClass
     * @param array  $data
     *
     * @return array
     */
    public function filter(ResourceEvent $event, $entityClass, $data)
    {
        // Verify the user has permission to create a new client
        $result = $this->validateOAuth2('client-create');
        if ($result instanceof ApiProblem) {
            return $result;
        }

        // Encrypt password
        $bcrypt = new Bcrypt();
        $bcrypt->setCost(14);
        $data->secret = $bcrypt->create($data->secret);

        // Set owning user to current user
        $userIdentity = $event->getIdentity()->getAuthenticationIdentity();
        $data->user = $userIdentity['user_id'];

        return $data;
    }
}
