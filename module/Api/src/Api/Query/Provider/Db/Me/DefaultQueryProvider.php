<?php

namespace Api\Query\Provider\Db\Me;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\ResourceEvent;
use OAuth2\Request as OAuth2Request;
use OAuth2\Server as OAuth2Server;
use ZF\Apigility\Doctrine\Server\Query\Provider\AbstractQueryProvider;

/**
 * Class FetchAllOrm
 *
 * @package ZF\Apigility\Doctrine\Server\Query\Provider
 */
class DefaultQueryProvider extends AbstractQueryProvider
{
    /**
     * @param string $entityClass
     * @param array  $parameters
     *
     * @return mixed This will return an ORM or ODM Query\Builder
     */
    public function createQuery(ResourceEvent $event, $entityClass, $parameters)
    {
        $userIdentity = $event->getIdentity()->getAuthenticationIdentity();
        $user = $userIdentity['user_id'];
        $md5 = 'a' . md5(rand());

        $queryBuilder = $this->getObjectManager()->createQueryBuilder();
        $queryBuilder->select('row')
            ->from($entityClass, 'row')
            ->andwhere("row.id = :$md5")
            ->setParameter($md5, $user)
        ;

        return $queryBuilder;
    }
}
