<?php
return array(
    'zf-apigility-doctrine-query-create-filter' => array(
        'invokables' => array(
            'oauth2_client' => 'Api\\Query\\CreateFilter\\OAuth2\\ClientCreateFilter',
        ),
    ),
    'zf-apigility-doctrine-query-provider' => array(
        'invokables' => array(
            'oauth2_client_default' => 'Api\\Query\\Provider\\OAuth2\\Client\\DefaultQueryProvider',
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'oauth2_client_create_listener' => 'Api\\EventListener\\OAuth2\\Client\\CreateListener',
        ),
        'aliases' => array(
            'ZF\\OAuth2\\Provider\\UserId' => 'Api\\Provider\\UserId\\ZfcAuthenticationService',
        ),
        'factories' => array(
            'Api\\Provider\\UserId\\ZfcAuthenticationService' => 'Api\\Provider\\UserId\\ZfcAuthenticationServiceFactory',
        ),
    ),
    'router' => array(
        'routes' => array(
            'api.rest.doctrine.o-auth2-client' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/oauth2/client[/:client_id]',
                    'defaults' => array(
                        'controller' => 'Api\\V1\\Rest\\OAuth2Client\\Controller',
                    ),
                ),
            ),
            'api.rpc.me' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/me',
                    'defaults' => array(
                        'controller' => 'Api\\V1\\Rpc\\Me\\Controller',
                        'action' => 'me',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'api.rest.doctrine.o-auth2-client',
            2 => 'api.rpc.me',
        ),
    ),
    'zf-rest' => array(
        'Api\\V1\\Rest\\OAuth2Client\\Controller' => array(
            'listener' => 'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientResource',
            'route_name' => 'api.rest.doctrine.o-auth2-client',
            'route_identifier_name' => 'client_id',
            'entity_identifier_name' => 'id',
            'collection_name' => 'o_auth2_client',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'ZF\\OAuth2\\Doctrine\\Entity\\Client',
            'collection_class' => 'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientCollection',
            'service_name' => 'OAuth2Client',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'Api\\V1\\Rest\\OAuth2Client\\Controller' => 'HalJson',
            'Api\\V1\\Rest\\User\\Controller' => 'HalJson',
            'Api\\V1\\Rpc\\Me\\Controller' => 'Json',
        ),
        'accept-whitelist' => array(
            'Api\\V1\\Rest\\OAuth2Client\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
            'Api\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content-type-whitelist' => array(
            'Api\\V1\\Rest\\OAuth2Client\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ),
            'Api\\V1\\Rest\\User\\Controller' => array(
                0 => 'application/json',
            ),
        ),
        'accept_whitelist' => array(
            'Api\\V1\\Rpc\\Me\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ),
        ),
        'content_type_whitelist' => array(
            'Api\\V1\\Rpc\\Me\\Controller' => array(
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'ZF\\OAuth2\\Doctrine\\Entity\\Client' => array(
                'route_identifier_name' => 'client_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.o-auth2-client',
                'hydrator' => 'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientHydrator',
            ),
            'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.o-auth2-client',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-apigility' => array(
        'doctrine-connected' => array(
            'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientResource' => array(
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientHydrator',
                'query_create_filter' => 'oauth2_client',
                'query_providers' => array(
                    'default' => 'oauth2_client_default',
                ),
                'listeners' => array(
                    0 => 'oauth2_client_create_listener',
                ),
            ),
        ),
    ),
    'doctrine-hydrator' => array(
        'Api\\V1\\Rest\\OAuth2Client\\OAuth2ClientHydrator' => array(
            'entity_class' => 'ZF\\OAuth2\\Doctrine\\Entity\\Client',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(),
            'use_generated_hydrator' => true,
        ),
        'Api\\V1\\Rest\\User\\UserHydrator' => array(
            'entity_class' => 'Db\\Entity\\User',
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => array(),
            'use_generated_hydrator' => true,
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'Api\\V1\\Rpc\\Me\\Controller' => 'Api\\V1\\Rpc\\Me\\MeControllerFactory',
        ),
    ),
    'zf-rpc' => array(
        'Api\\V1\\Rpc\\Me\\Controller' => array(
            'service_name' => 'Me',
            'http_methods' => array(
                0 => 'GET',
            ),
            'route_name' => 'api.rpc.me',
        ),
    ),
    'zf-content-validation' => array(
        'Api\\V1\\Rest\\User\\Controller' => array(
            'input_filter' => 'Api\\V1\\Rest\\User\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'Api\\V1\\Rest\\User\\Validator' => array(
            0 => array(
                'name' => 'username',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(),
            ),
            1 => array(
                'name' => 'password',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(),
            ),
            2 => array(
                'name' => 'email',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(),
            ),
            3 => array(
                'name' => 'displayName',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(),
            ),
            4 => array(
                'name' => 'state',
                'required' => false,
                'filters' => array(
                    0 => array(
                        'name' => 'Zend\\Filter\\StringTrim',
                    ),
                    1 => array(
                        'name' => 'Zend\\Filter\\StripTags',
                    ),
                ),
                'validators' => array(),
            ),
            5 => array(
                'name' => 'createdAt',
                'required' => false,
                'filters' => array(),
                'validators' => array(),
            ),
        ),
    ),
);
