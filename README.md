Apigility OAuth2 Doctrine Skeleton
==================================


Install
-------

```sh
php composer.phar install
cp config/autoload/local.php.dist config/autoload/local.php
php public/index.php orm:schema-tool:create
php public/index.php data-fixture:import
cd public
bower install
cd ..
```


Run
---


Create a new administrator
```
php public/index.php skeleton:create:administrator
```


Run the server locally
```
php -S localhost:8081 -t public public/index.php
```


Using the email and password entered for the administrator fetch an access token
```
http --auth <email>:<password> -f POST http://localhost:8081/oauth grant_type=client_credentials
```


Create a new client (app) for the administrator
```
echo '{"clientId": "clienttest", "secret": "password"}' | http -f POST http://localhost:8081/api/oauth2/client "Content-Type: application/json" "Authorization: Bearer <access_token>"
```

